from typing import Callable, Generic, Protocol, TypeVar

from dataclasses import dataclass
from uuid import UUID

from thespian.actors import ActorSystem, ActorTypeDispatcher, ActorAddress


EventType = TypeVar('EventType')
EventProducer = Callable[[list[EventType]], list[EventType]]


@dataclass(frozen=True)
class Append(Generic[EventType]):
    stream: UUID
    events: list[EventType]


class Get:
    pass


@dataclass(frozen=True)
class GetStream(Generic[EventType]):
    stream: UUID


@dataclass(frozen=True)
class Evolve(Generic[EventType]):
    stream: UUID
    eventProducer: EventProducer[EventType]


class Mailbox(Generic[EventType], ActorTypeDispatcher):

    history: dict[UUID, list[EventType]] = {}

    def receiveMsg_Get(self, message: Get, sender: ActorAddress) -> None:
        self.send(sender, self.history)

    def receiveMsg_GetStream(self, message: GetStream,
            sender: ActorAddress) -> None:
        self.send(sender, self._get_stream_events(message.stream))

    def receiveMsg_Append(self,
            message: Append,
            sender: ActorAddress) -> None:
        self.history[message.stream] = self._get_stream_events(
            message.stream) + message.events

    def receiveMsg_Evolve(self, message: Evolve[EventType],
            sender: ActorAddress) -> None:
        stream_events = self._get_stream_events(message.stream)
        new_events = message.eventProducer(stream_events)
        self.history[message.stream] = stream_events + new_events

    def _get_stream_events(self, stream: UUID):
        return self.history.get(stream, [])


class EventStore(Generic[EventType]):

    actor_system = ActorSystem()
    mailbox = actor_system.createActor(Mailbox)

    def get(self) -> dict[UUID, list[EventType]]:
        return self.actor_system.ask(self.mailbox, Get())

    def getStream(self, stream: UUID) -> list[EventType]:
        return self.actor_system.ask(self.mailbox, GetStream(stream=stream))

    def append(self, stream: UUID, events: list[EventType]) -> None:
        self.actor_system.tell(self.mailbox,
            Append(stream=stream, events=events))

    def evolve(self, stream: UUID, eventProducer: EventProducer) -> None:
        self.actor_system.tell(self.mailbox,
            Evolve(stream=stream, eventProducer=eventProducer))


State = TypeVar('State')


class Projection(Protocol, Generic[EventType, State]):

    @classmethod
    def init(cls) -> State:
        raise NotImplementedError()

    @classmethod
    def update(cls, state: State, event: EventType) -> State:
        raise NotImplementedError()

    @classmethod
    def project(cls, events: list[EventType]) -> State:
        state = cls.init()
        for event in events:
            state = cls.update(state, event)
        return state
