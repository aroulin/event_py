from flask import Flask

from . import routes
from .utils import init_store


def create_app():
    app = Flask(__name__)

    app.register_blueprint(routes.app)
    init_store(routes.event_store)

    return app
