from typing import Union

from collections import defaultdict
from dataclasses import dataclass
from enum import Enum, unique

from .store import EventProducer, Projection


@unique
class Flavour(str, Enum):
    Strawberry = 'strawberry'
    Vanilla = 'vanilla'


@dataclass(frozen=True)
class FlavourSold:
    flavour: Flavour


@dataclass(frozen=True)
class FlavourRestocked:
    flavour: Flavour
    quantity: int  # TODO assert positive


@dataclass(frozen=True)
class FlavourOutOfStock:
    flavour: Flavour


@dataclass(frozen=True)
class FlavourNotInStock:
    flavour: Flavour


Event = Union[
    FlavourSold,
    FlavourRestocked,
    FlavourOutOfStock,
    FlavourNotInStock,
    ]


class SoldFlavours(Projection):

    Sales = dict[Flavour, int]

    @classmethod
    def init(cls) -> Sales:
        return defaultdict(int)

    @classmethod
    def update(cls, state: Sales, event: Event) -> Sales:
        flavour = event.flavour
        if isinstance(event, FlavourSold):
            state[flavour] = state[flavour] + 1
        return state


class FlavoursInStock(Projection):

    Stock = dict[Flavour, int]

    @classmethod
    def init(cls) -> Stock:
        return defaultdict(int)

    @classmethod
    def update(cls, state: Stock, event: Event) -> Stock:
        flavour = event.flavour
        if isinstance(event, FlavourSold):
            cls.restock(flavour, -1, state)
        elif isinstance(event, FlavourRestocked):
            cls.restock(flavour, event.quantity, state)
        return state

    @classmethod
    def restock(cls, flavour: Flavour, quantity: int, state: Stock) -> Stock:
        state[flavour] = state[flavour] + quantity
        return state


def sell(flavour: Flavour) -> EventProducer:
    def producer(events: list[Event]) -> list[Event]:
        flavoursInStock = FlavoursInStock()
        stock = flavoursInStock.project(events)[flavour]

        if stock == 0:
            return [FlavourNotInStock(flavour=flavour)]
        if stock == 1:
            return [
                FlavourSold(flavour=flavour),
                FlavourOutOfStock(flavour=flavour),
                ]
        return [FlavourSold(flavour=flavour)]

    return producer


def restock(flavour: Flavour, quantity: int) -> EventProducer:
    def producer(events: list[Event]) -> list[Event]:
        return [FlavourRestocked(flavour=flavour, quantity=quantity)]

    return producer
