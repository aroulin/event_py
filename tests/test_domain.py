from hypothesis import strategies as st
from hypothesis.stateful import Bundle, RuleBasedStateMachine, rule

from event_py.domain import sell, restock, Flavour
from event_py.domain import FlavourNotInStock, FlavourOutOfStock
from event_py.domain import FlavourRestocked, FlavourSold
from event_py.domain import FlavoursInStock


class EventStoreModel(RuleBasedStateMachine):

    Events = Bundle('events')

    @rule(target=Events)
    def new_events(self):
        return []

    @rule(
        target=Events,
        events=Events,
        flavour=st.sampled_from(Flavour),
        quantity=st.integers(min_value=0),
        )
    def restock_flavour(self, events, flavour, quantity):
        stock = FlavoursInStock().project(events)[flavour]
        producer = restock(flavour, quantity)
        new_events = producer(events)
        assert new_events == [
            FlavourRestocked(flavour=flavour, quantity=quantity),
            ]
        events += new_events
        new_stock = FlavoursInStock().project(events)[flavour]
        assert new_stock == stock + quantity
        return events

    @rule(
        target=Events,
        events=Events,
        flavour=st.sampled_from(Flavour),
        )
    def sell_flavour(self, events, flavour):
        stock = FlavoursInStock().project(events)[flavour]
        producer = sell(flavour)
        new_events = producer(events)
        events += new_events
        new_stock = FlavoursInStock().project(events)[flavour]
        if stock > 1:
            assert new_events == [FlavourSold(flavour=flavour)]
            assert new_stock == stock - 1
        if stock == 1:
            assert new_events == [
                FlavourSold(flavour=flavour),
                FlavourOutOfStock(flavour=flavour),
                ]
            assert new_stock == 0
        if stock == 0:
            assert new_events == [FlavourNotInStock(flavour=flavour)]
            assert new_stock == 0
        return events


TestEventStore = EventStoreModel.TestCase
