from typing import Any

from dataclasses import asdict
from uuid import uuid4

from .domain import Flavour, restock, sell
from .store import EventType


def jsonify(event: EventType) -> dict[str, Any]:
    return {
        'type': type(event).__name__,
        'data': asdict(event),
        }


def init_store(store):
    truck1 = uuid4()
    truck2 = uuid4()

    store.evolve(truck1, sell(Flavour.Vanilla))
    store.evolve(truck1, sell(Flavour.Strawberry))
    store.evolve(truck1, restock(Flavour.Strawberry, 3))
    store.evolve(truck1, sell(Flavour.Strawberry))

    store.evolve(truck2, sell(Flavour.Vanilla))
    store.evolve(truck2, sell(Flavour.Strawberry))
    store.evolve(truck2, restock(Flavour.Vanilla, 3))
    store.evolve(truck2, sell(Flavour.Strawberry))
