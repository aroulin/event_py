from typing import Any

from uuid import UUID

from flask import Blueprint

from .domain import Event, FlavoursInStock, SoldFlavours
from .store import EventStore
from .utils import jsonify

app = Blueprint('routes', __name__)
event_store = EventStore[Event]()


@app.route('/')
def events() -> dict[str, Any]:
    return {
        str(streamId): [jsonify(event) for event in stream]
        for streamId, stream in event_store.get().items()
        }


@app.route('/<uuid:stream>/sales')
def sales(stream: UUID) -> dict[str, int]:
    sold = SoldFlavours().project(event_store.getStream(stream))
    return {
        flavour.value: quantity for flavour, quantity in sold.items()
        }


@app.route('/<uuid:stream>/stock')
def stock(stream: UUID) -> dict[str, int]:
    stock = FlavoursInStock().project(event_store.getStream(stream))
    return {
        flavour.value: quantity for flavour, quantity in stock.items()
        }
